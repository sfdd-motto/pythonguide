# grep_ex.py

import argparse
import re

from pathlib import Path


parser = argparse.ArgumentParser(description="grep command")

# positional argument when defined without --
# nargs='?' is required for default-positional-arg values
parser.add_argument('pattern', type=str, nargs=1) 
parser.add_argument('location', type=str, nargs='*') 


args = parser.parse_args() # save arguments in args
loc = Path(args.location[0]) # save location
ptrn = args.pattern[0] # save pattern

lines = open(loc).readlines();

for line in lines:
    if re.compile(ptrn).search(line):
        print(line, end="")
