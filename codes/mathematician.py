# mathematician.py
# user-1: using Ring.py for mathematical analysis

from random import random, seed
from pythonic import Ring

def calc_avg_area(n=5, seed_value=3):
    # seed for random number generator 
    seed(seed_value)

    # random radius
    rings = [Ring(radius=random()) for i in range(n)]

    total = 0
    for r in rings:
        total += r.area()
        # # print values for each iteration 
        print("%0.2f, %0.2f, %0.2f" % (r.radius, r.area(), total))
    avg_area = sum([r.area() for r in rings])/n
    
    return avg_area

def expansion(radius=1.0, expansion=2.0):
        radius *= expansion  # 2.0 times radius expansion due to heat 
        return radius

def main():
    # # generate 'n' rings
    # n = 10
    # avg_area = calc_avg_area(n=10)
    # print("\nAverage area for n={0} is n/{0} = {1:.2f}".format(n, avg_area))

    radii = [1, 3, 5] # list of radius
    rings = [Ring(radius=r) for r in radii] # create object of different radius
    for r in rings:
        print("Radius:", r.radius)
        print("Perimeter at room temperature: %0.2f" % r.perimeter())
        # radius after expansion
        r.radius = expansion(r.radius) # modifying the attribute of the class
        print("Perimeter after heating:, %0.2f" % r.perimeter())

if __name__ == '__main__':
    main()
