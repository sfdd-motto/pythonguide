# box.py
# user-3 : creating boxes for the ring

from pythonic import Ring

class Box(Ring):
    """ Modified perimeter for creating the box """

    def perimeter(self): # override the method 'perimeter'
        # perimeter is increased 2.0 times
        return Ring.perimeter(self) * 2.0 

    __perimeter = perimeter

def main():
    b = Box(radius=8) # pass radius = 8
    print("Radius:", b.radius)
    print("Modified perimeter: %0.2f" % b.perimeter()) # (2*pi*radius) * 2

if __name__ == '__main__':
    main()
